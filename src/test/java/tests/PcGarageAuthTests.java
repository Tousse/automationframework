package tests;

import Utils.Util;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;
import pageFactory.PcGarageAuthPage;

public class PcGarageAuthTests extends BaseTest {
    @Test
    public void authenticateTest01() {
        driver.get("https://www.pcgarage.ro/autentificare/");
        PcGarageAuthPage auth = PageFactory.initElements(driver, PcGarageAuthPage.class);
        auth.loginPage("abc@gmail.com", "abc123");
    }

    @Test
    public void registerTest01() {
        driver.get("https://www.pcgarage.ro/autentificare/");
        String username = Util.getRandomEmail();
        String password = Util.getRandomString(5);
        PcGarageAuthPage auth = PageFactory.initElements(driver, PcGarageAuthPage.class);
        auth.register("ABC",
                "CDE",
                "0722222222",
                username,
                password);
    }

}
