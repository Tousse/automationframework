package tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageObjects.AuthenticationPage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class AuthenticationTests extends tests.BaseTest {

    @DataProvider(name = "negativeData")
    public Iterator<Object[]> dataNegatives() {
        Collection<Object[]> dp = new ArrayList<Object[]>();
        // username, password, username error, password error, general error
        dp.add(new String[]{"", "", "Please enter your username", "Please enter your password", ""});
        dp.add(new String[]{"aaaaaa", "", "", "Please enter your password", ""});
        dp.add(new String[]{"", "aaaaa", "Please enter your username", "", ""});
        dp.add(new String[]{"aaaa", "aaaa", "", "", "Incorrect username or password"});
        return dp.iterator();
    }

    @DataProvider(name = "positiveData")
    public Iterator<Object[]> positiveData() {
        Collection<Object[]> dp = new ArrayList<Object[]>();
        // username, password, browser
        dp.add(new String[]{"dinosaur", "dinosaurpassword", "chrome"});
        dp.add(new String[]{"dingo", "dingopassword", "chrome"});
        dp.add(new String[]{"camel", "camelpassword", "firefox",});
        return dp.iterator();
    }

    @Test(dataProvider = "negativeData")
    public void authNegativeTest(String username, String password, String userError, String passError, String generalErr) {
        //setDriver("chrome");
        driver.get("http://" + hostname + "/stubs/auth.html");
        // Finding elements on page
        AuthenticationPage authenticationPage = new AuthenticationPage(driver);

        // Inserting username and pass and click on submit
        authenticationPage.login(username, password);

        // Finding errors on the page
        Assert.assertTrue(authenticationPage.checkForError(userError,"userErr"));
        Assert.assertTrue(authenticationPage.checkForError(passError,"passErr"));
        Assert.assertTrue(authenticationPage.checkForError(generalErr,"generalErr"));
    }

    @Test(dataProvider = "negativeData")
    public void authNegativeTestFactory(String username, String password, String userError, String passError, String generalErr) {
        //setDriver("chrome");
        driver.get("http://" + hostname + "/stubs/auth.html");
        // Finding elements on page
        pageFactory.AuthenticationPage authPage = PageFactory.initElements(driver, pageFactory.AuthenticationPage.class);
        //pageFactory.AuthenticationPage authPage = new pageFactory.AuthenticationPage(driver);

        // Inserting username and pass and click on submit
        authPage.login(username, password);

        // Finding errors on the page
        Assert.assertTrue(authPage.checkForError(userError,"userErr"));
        Assert.assertTrue(authPage.checkForError(passError,"passErr"));
        Assert.assertTrue(authPage.checkForError(generalErr,"generalErr"));
    }

    @Test(dataProvider = "positiveData")
    public void authPositiveTest(String username, String password, String browser) {
        //setDriver(browser);
        driver.get("http://" + hostname + "/stubs/auth.html");
        AuthenticationPage authenticationPage = new AuthenticationPage(driver);
        // Inserting username and pass and click on submit
        authenticationPage.login(username, password);

        //verify login page appear
        WebElement userNameElem = waitForGenericElement(By.id("user"), 10);
        Assert.assertEquals(username, userNameElem.getText());
        //click logout
        authenticationPage.logOut();
    }
}
