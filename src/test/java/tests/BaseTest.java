package tests;

import Utils.Util;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import static Utils.SeleniumUtils.getDriver;

public class BaseTest {
    WebDriver driver;
    //String hostname= "192.168.1.106";
    String hostname;

//    String browserType = "chrome";
//
    @BeforeTest
    public void beforeTest() {
        // Method 1 -D cmd line parameters
        System.out.println(System.getProperty("browserType"));

        // Method 2 OS Environment variable
        System.out.println("ENV BROWSER Value: " + System.getenv("browserType"));

        // Method 3 Property file
        try
        {
            InputStream input = new FileInputStream("src\\test\\java\\framework.properties");
            Properties prop = new Properties();
            prop.load(input);
            driver = getDriver(prop.getProperty("browserType"));
            System.out.println("PROPERTY BROWSER Value: " + prop.getProperty("browserType"));
            hostname = prop.getProperty("hostname");
        }
        catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    @AfterTest(alwaysRun = true)
    public void closeBrowserAtEnd() {
        System.out.println("Close driver on AfterTest");
        driver.quit();
    }

//    @AfterMethod(alwaysRun = true)
//    public void closeBrowser() {
//        System.out.println("Close driver on AfterMethod");
//        //driver.quit();
//    }


    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public WebElement waitForGenericElement(By by, int timeout) {
        return Util.waitForGenericElement(driver, by, timeout);
    }
}
