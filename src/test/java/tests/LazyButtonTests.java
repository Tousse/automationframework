package tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class LazyButtonTests extends BaseTest {

    // implicit wait
    @Test
    public void lazyButtonTest1() {
        driver.get("http://" + hostname + "/stubs/lazy.html");
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        WebElement lazyButton = driver.findElement(By.id("lazy-button"));
        lazyButton.click();
        lazyButton = driver.findElement(By.id("lazy-button"));
        lazyButton.click();
    }

    @Test
    public void lazyButtonTest2() {
        driver.get("http://" + hostname + "/stubs/lazy.html");
        for(int i = 0 ; i < 3 ; i ++) {
            waitForGenericElement(By.id("lazy-button"), 10).click();
        }
    }

    @Test
    public void lazyButtonTest4() {
        driver.get("http://" + hostname + "/stubs/lazy.html");
        WebDriverWait wait = new WebDriverWait(driver, 10);
        for(int i = 0 ; i < 3 ; i ++) {
            WebElement lazyButton = wait.until(ExpectedConditions.
                    presenceOfElementLocated(By.id("lazy-button")));

            lazyButton.click();
        }
    }

    // DO NOT USE !!!
    @Test
    public void lazyButtonTest3() {
        driver.get("http://" + hostname + "/stubs/lazy.html");
        //WebDriverWait wait = new WebDriverWait(driver, 10);
        for(int i = 0 ; i < 3 ; i ++) {
            WebElement lazyButton = driver.findElement(By.id("lazy-button"));
            lazyButton.click();
            try {
                Thread.sleep(10000);
            }
            catch (Exception ex) {

            }
        }
    }

}
