package Utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Random;

public class Util {

    public static WebElement waitForGenericElement(WebDriver driver, By by, int timeout) {
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        return wait.until(ExpectedConditions.
                presenceOfElementLocated(by));
    }

    public static String getRandomString(int size) {
        final String alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String results = "";
        for (int i = 0 ; i < size ; i++) {
            Random rnd = new Random();
            results = results + alphabet.charAt(rnd.nextInt(alphabet.length()));
        }
        return results;
    }

    public static String getRandomEmail() {
        String user = getRandomString(10);
        String domain = "gmail";
        String tld = "com";
        StringBuilder sb = new StringBuilder();
        //System.out.println(user);
        return sb.append(user).append("@").append(domain).append(".").append(tld).toString();
    }
}
