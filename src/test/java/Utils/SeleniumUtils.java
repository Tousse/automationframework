package Utils;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class SeleniumUtils {
    public static WebDriver getDriver(String browserType) {
        WebDriver driver = null;
        if (browserType.toUpperCase().equals(Browsers.CHROME.toString())) {
            WebDriverManager.chromedriver().setup();
            driver = new ChromeDriver();
        }
        if (browserType.toUpperCase().equals(Browsers.FIREFOX.toString())) {
            WebDriverManager.firefoxdriver().setup();
            driver = new FirefoxDriver();
        }
        if (browserType.toUpperCase().equals(Browsers.IE.toString())) {
            WebDriverManager.iedriver().setup();
            driver = new InternetExplorerDriver();
        }
        if (driver == null) {
            System.out.println("WARNING the driver is null because the selection did not match an existing browser !!");
        }
        return driver;
    }
    public static WebDriver getDriver(Browsers browserType) {
        return getDriver(browserType.toString());
    }

}
