package course1;

public class Rectangle {

    private double width, length;

    public double getArea() {
        return width * length;
    }

    public double getPerimeter() {
        return 2 * ( width + length);
    }

    public double getDiagonal() {
        return Math.sqrt( width * width + length * length);
    }

    public Rectangle(double width, double length) {
        this.length = length;
        this.width = width;
    }
}
