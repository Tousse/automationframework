package course1;

public class Shapes {
    public static void main(String[] args) {
        Square sq = new Square();
        sq.setSide(Double.parseDouble(args[0]));
        System.out.println(sq.getArea());

        for (String s : args) {
            Square square = new Square();
            square.setSide(Double.parseDouble(s));
            System.out.println(square.getArea());
        }

        Square sq2 = new Square(Double.parseDouble(args[0]));
        System.out.println(sq2.getArea());

    }
}
