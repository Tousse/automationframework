package course1;

public class PrintStars {

    public static void main(String[] args){
        drawShapeCorners(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
    }

    public static void drawFullShape(int width, int height) {// implement method
        for (int i = 0 ; i < width ; i++) {
            for (int j = 0; j < height; j++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }

    public static void drawShapeOutline(int width, int height) {
        for (int i = 0 ; i < width ; i++) {
            for (int j = 0; j < height; j++) {
                if (i==0 || i == width -1) {
                    System.out.print("*");
                }
                else {
                    if (j==0 || j == height -1) {
                        System.out.print("*");
                    }
                    else {
                        System.out.print(" ");
                    }
                }
            }
            System.out.println();
        }
    }

    public static void drawShapeCorners(int width, int height) {
        for (int i = 0 ; i < width ; i++) {
            for (int j = 0; j < height; j++) {
                if ((i==0 && j==0) ||
                        (i==0 && j == height-1) ||
                        (i==width-1 && j==0) ||
                        (i==width-1 && j == height-1)) {
                    System.out.print("*");
                }
                else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }
}
