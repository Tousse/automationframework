package course1;

import java.awt.*;

public class Car {
    private Color color;
    private byte gear;
    private float fuelLevel;
    private float speed;
    private boolean state;
    private float direction;

    public void accelerate(float speedDelta) {
        if (speedDelta < 0) {
            if (speed + speedDelta < 0) {
                speed = 0;
                return;
            }
        }
        this.speed += speedDelta; // speed = speed + speedDelta
    }

    public void steer(float angle) {
        accelerate(-10);
        gearDown();
        direction =+ angle;
        direction = direction % 360;
    }

    public void gearUp() {
        gear++;
    }

    public void gearDown() {
        if (gear > 0) {
            gear--;
        }
    }

    public void start() {
        state = true;
        gearUp();
        accelerate(1);
    }

    public void stop() {
        state = false;
        gear = 0;
        speed = 0;
    }

    public void steerLeft() {
        steer(90);
    }

    public void steerRight() {
        steer(-90);
    }
}
