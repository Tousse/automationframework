package course1;

public class Hello {

    final static float PI = 3.1415f;
    final int MAX_NUMBER_OF_STUDENTS = 20;

    public static void main(String[] args) {
        int number1, number2;
        char c = 'A';
        System.out.println("Hello world! ");
        float f = 2.0f;

        System.out.println("Argument: " + args[0]);
        int a = Integer.parseInt(args[0]);

        System.out.println("Argument: " + args[1]);
        int b = Integer.parseInt(args[1]);

        System.out.println(args[0] + args[1]);
        System.out.println(a + b);

        System.out.println(100/3);
        System.out.println(100.0/3);
        System.out.println("FOR loop");
        for (int i = 0; i < args.length ; i++) {
            System.out.println("Argumentul " + i + " este: " + args[i]);
            System.out.println("Argumentul " + i + " lungime: " + args[i].length());
            if (Integer.parseInt(args[i]) > 50 && Integer.parseInt(args[i]) <100 ) {
                System.out.println(args[i] + " este intre 50 si 100");
            }

            if (Integer.parseInt(args[i]) > 50) {
                if ( Integer.parseInt(args[i]) <100 ) {
                    System.out.println(args[i] + " este intre 50 si 100");
                }
            }
        }
        System.out.println("While loop");
        int i = 0;
        while(i < args.length) {
            System.out.println("Argumentul " + i + " este: " + args[i]);
            i++;
        }
        System.out.println("while with break loop");
        int j = 0;
        while(true) {
            if (j < args.length) {
                System.out.println("Argumentul " + j + " este: " + args[j]);
                j++;
            }
            else {
                break;
            }
        }
        System.out.println("FOREACH loop");
        for (String s : args) {
            System.out.println("Argumentul este: " + s);

        }

    }

}
