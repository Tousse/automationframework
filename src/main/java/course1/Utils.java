package course1;

public class Utils {

    public static int a = 0;

    public static void splitString(String s) {
        for (String s1 : s.split(" ")) {
            System.out.println(s1);
        }
    }

    public void splitStringNonStatic(String s) {
        for (String s1 : s.split(" ")) {
            System.out.println(s1);
        }
    }

}
