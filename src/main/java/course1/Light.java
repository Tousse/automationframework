package course1;

public class Light {

    private boolean state;
    private float intensity;

    // turn on the ligh, set the intensity to 10
    public void turnOn() {
        state = true;
        this.intensity = 10;
    }

    // turn off the light, intensity is 0
    public void turnOff() {
        state = false;
        this.intensity = 0;
    }

    public void dim(float intensity) {
        if (this.intensity - intensity < 0) {
            this.intensity = 0;
            turnOff();
        }
        else {
            this.intensity -= intensity; // this.intensity = this.intensity - intensity
        }
    }

    public void brighten(float intensity) {
        this.intensity += intensity;
    }

//    // getter
//    public Boolean getState() {
//        return this.state;
//    }
//
//    // setter
//    public void setState(Boolean state) {
//        this.state = state;
//    }


    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    public float getIntensity() {
        return intensity;
    }

    public void setIntensity(float intensity) {
        this.intensity = intensity;
    }

    public Light() {
        this.state = state;
        this.intensity = intensity;
    }
}
